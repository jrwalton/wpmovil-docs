
## API REST Walton Movil
Documentacion de la API REST backend utilizada para la app movil de Walton.

#### Recursos:
- [Accesos (Login)](#login)
- [Usuarios](#usuarios)
- [Operaciones (Ventas)](#operaciones)

#### Login
**Endpoint**: /wpmovil/login
<details><summary><b>Parametros y Respuestas</b></summary>
<br>
<details><summary><b>Parametros:</b></summary>
<p>Caso 1: Usuario Cliente de Comercio: username no necesario</p>

```json
{
  "celular": "0976919797",
  "nroCedula": "5780093.",
  "password": "string",
}

```

<p>Caso 2: Usuario Comercio: se necesita especificar el username</p>

```json
{
  "celular": "0976919797",
  "nroCedula": "5780093.",
  "password": "string",
  "username": ""
}

```

</details>

<details>
<summary><b>Posibles Respuestas:</b></summary>

```json
{
  "celular": "0976919797",
  "nroCedula": "578009.",
  "password": "string",
  "username": "string"
}

```

</details>
</details>

<details><summary><b>Datos de Prueba</b></summary>

Usuario Comercio:

```json
{
  "celular": "0976919797",
  "nroCedula": "578009.",
  "password": "string",
  "username": "string"
}

```

Usuario Cliente de Comercio:

```json
{
  "celular": "0971456456",
  "nroCedula": "456789",
  "password": "lmorel"
}

```
</details>


#### Usuarios
**Endpoint**: /wpmovil/usuarios
<details><summary><b>Parametros y Respuestas</b></summary>
<br>
<details><summary><b>Parametros:</b></summary>
<p>Caso 1: Usuario Cliente de Comercio: username no necesario</p>

```json
{
  "celular": "0976919797",
  "nroCedula": "5780093.",
  "password": "string",
}

```

<p>Caso 2: Usuario Comercio: se necesita especificar el username</p>

```json
{
  "celular": "0976919797",
  "nroCedula": "5780093.",
  "password": "string",
  "username": ""
}

```

</details>

<details>
<summary><b>Posibles Respuestas:</b></summary>

```json
{
  "celular": "0976919797",
  "nroCedula": "578009.",
  "password": "string",
  "username": "string"
}

```

</details>
</details>

<details><summary><b>Datos de Prueba</b></summary>

Usuario Comercio:

```json
{
  "celular": "0976919797",
  "nroCedula": "578009.",
  "password": "string",
  "username": "string"
}

```

Usuario Cliente de Comercio:

```json
{
  "celular": "0976919797",
  "nroCedula": "578009.",
  "password": "string",
  "username": "string"
}

```
</details>
